<?php

namespace Plugins\Forms\Entities;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    /**
     * Table name on database
     *
     */
    protected $table = 'forms_fields';

    /**
     * Guarded fields
     *
     */
    protected $guarded = [];

    /**
     * BelongsTo Form
     *
     */
    public function form()
    {
        return $this->belongsTo(Form::class);
    }
}
