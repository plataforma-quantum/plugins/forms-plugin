<?php

namespace Plugins\Forms\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Email extends Model
{
    use Notifiable;

    /**
     * Table name on database
     *
     */
    protected $table = 'forms_emails';

    /**
     * Guarded model fields
     *
     */
    protected $guarded = [];

    /**
     * BelongsTo Form
     *
     */
    public function form()
    {
        return $this->belongsTo(Form::class);
    }
}
