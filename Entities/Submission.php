<?php

namespace Plugins\Forms\Entities;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    /**
     * Model table name
     *
     */
    protected $table = 'forms_submissions';

    /**
     * Guarded fields
     *
     */
    protected $guarded = [];

    /**
     * BelongsTo Form
     *
     */
    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    /**
     * BelongsToMany Field
     *
     */
    public function fields()
    {
        return $this->belongsToMany(Field::class, 'forms_submissions_fields')->withPivot(['value']);
    }
}
