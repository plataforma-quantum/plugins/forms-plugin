<?php

namespace Plugins\Forms\Entities;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    /**
     * Table name on database
     *
     */
    protected $table = 'forms_forms';

    /**
     * Guarded model properties
     *
     */
    protected $guarded = [];

    /**
     * published Model Scope
     *
     */
    public function scopePublished($query)
    {
        return $query->where('status', 'like', '%published%');
    }

    /**
     * HasMany Field
     *
     */
    public function fields()
    {
        return $this->hasMany(Field::class);
    }

    /**
     * HasMany Submission
     *
     */
    public function submissions()
    {
        return $this->hasMany(Submission::class);
    }

    /**
     * HasMany Emails
     *
     */
    public function emails()
    {
        return $this->hasMany(Email::class);
    }
}
