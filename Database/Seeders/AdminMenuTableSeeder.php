<?php

namespace Plugins\Forms\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menu')->where('plugin', 'forms')->delete();
        DB::table('admin_menu')->insertGetId([
            'parent_id' => 0,
            'order' => 80,
            'title' => 'Gerenciador de Formulários',
            'plugin' => 'forms',
            'icon' => 'fa-wpforms',
            'uri' => 'forms',
            'permission' => NULL,
            'created_at' => NULL,
            'updated_at' => '2020-06-01 17:54:28'
        ]);
    }
}
