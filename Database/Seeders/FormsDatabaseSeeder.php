<?php

namespace Plugins\Forms\Database\Seeders;

use Illuminate\Database\Seeder;

class FormsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminMenuTableSeeder::class);
    }
}
