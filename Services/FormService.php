<?php

namespace Plugins\Forms\Services;

use Illuminate\Support\Facades\Notification;
use Plugins\Forms\Entities\Form;
use Plugins\Forms\Notifications\FormSubmitted;
use Quantum\Models\Service;

class FormService extends Service
{

    /**
     * Service model instance
     *
     */
    protected $model = Form::class;

    protected $submissionService;

    public function __construct()
    {
        $this->submissionService = _q('forms')->service('submission');
    }

    /**
     * Gets a form by name
     *
     */
    public function findByName(string $name)
    {
        return $this->published()->where('name', 'like', $name)->first();
    }

    /**
     * Save a form submission
     *
     */
    public function saveSubmission(string $name, array $data)
    {
        $form = $this->findByName($name);
        $submission = $this->submissionService->create(['form_id' => $form->id]);

        foreach ($data as $name => $value) {
            $field = $form->fields()->where('name', 'like', $name)->first();
            $submission->fields()->save($field, [
                'value' => $value
            ]);
        }
        Notification::send($submission->form->emails, new FormSubmitted($submission));
    }
}
