<?php

namespace Plugins\Forms\Services;

use Quantum\Models\Service;
use Plugins\Forms\Entities\Submission;

class SubmissionService extends Service
{

    /**
     * Service model instance
     *
     */
    protected $model = Submission::class;
}
