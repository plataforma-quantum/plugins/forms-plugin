<?php

namespace Plugins\Forms\Http\Controllers\Admin;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Table;

class FormsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Formulários';

    /**
     * Form Service Instance
     *
     */
    protected $formService;

    /**
     * Constructor Method
     *
     */
    public function __construct()
    {
        $this->formService = _q('forms')->service('form');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid($this->formService->getModel());

        $grid->column('id', __('Código'))->sortable();
        $grid->column('title', __('Título'));
        $grid->column('name', __('Nome'));
        $grid->column('submissions', __('Submissões'))->display(fn ($submissions) => count($submissions));
        $grid->column('status', __('Status'))
            ->using(['draft' => 'Rascunho', 'published' => 'Publicado'])
            ->label([
                'draft' => 'default',
                'published' => 'success'
            ]);
        $grid->column('created_at', __('Criado'));
        $grid->column('updated_at', __('Atualizado'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show($this->formService->findOrFail($id));

        $show->field('id', __('Código'));
        $show->field('title', __('Título'));
        $show->field('name', __('Nome'));
        $show->field('status', __('Status'));
        $show->field('created_at', __('Criado'));
        $show->field('updated_at', __('Atualizado'));
        $show->submissions(__('Submissões'), function ($grid) {
            $grid->id();
            $grid->disableActions();
            $grid->disableCreateButton();
            $grid->disableFilter();
            $grid->disableRowSelector();
            $grid->disableRowSelector();
            $grid->disableColumnSelector();
            $grid->disableTools();
            $grid->disableExport();
            $grid->column('form.title', __('Formulário'))->modal(__('Conteúdo'), function ($model) {

                $fields = $model->fields->map(function ($field) {
                    return [
                        'label' => $field->label,
                        'value' => $field->pivot->value
                    ];
                });
                return new Table([__('Campo'), __('Valor')], $fields->toArray());
            });
            $grid->created_at(__('Criado'))->date('H:i d/m/Y');
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form($this->formService->getModel());

        $form
            ->tab('Formulário', function ($form) {
                $form->text('title', __('Título'))->required();
                $form->text('name', __('Nome'))->required();
                $form->select('status', __('Status'))->options([
                    'draft' => 'Rascunho',
                    'published' => 'Publicado'
                ])->required();

                $form->hasMany('fields', 'Campos', function ($form) {
                    $form->text('label', 'Label')->required();
                    $form->text('name', 'Nome')->required();
                });
            })
            ->tab('Lista de Transmissão', function ($form) {
                $form->hasMany('emails', 'E-mails', function ($form) {
                    $form->email('email', 'E-mail')->required();
                });
            });

        return $form;
    }
}
