<?php

return [

    /**
     * Plugin name
     *
     */
    'name' => 'Forms',

    /**
     * Plugin services
     *
     */
    'services' => [
        'form' => Plugins\Forms\Services\FormService::class,
        'submission' => Plugins\Forms\Services\SubmissionService::class,
    ]
];
